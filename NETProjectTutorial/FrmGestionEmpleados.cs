﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleados : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;

        public DataSet DsEmpleados { set { dsEmpleados = value; } }

        public FrmGestionEmpleados()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
            dgvempleados.DataSource = bsEmpleados;
            dgvempleados.AutoGenerateColumns = true;
        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            FrmProducto fp = new FrmProducto();
            fp.TblProductos = dsEmpleados.Tables["Producto"];
            fp.DsProducto = dsEmpleados;
            fp.ShowDialog();
        }

        private void dgvempleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
